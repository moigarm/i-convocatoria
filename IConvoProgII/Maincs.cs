﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IConvoProgII
{
    public partial class Maincs : Form
    {
        

        public Maincs()
        {
            InitializeComponent();
        }
        

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void verInventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            
            f.MdiParent = this;
            f.Show();
        }

        private void reporteDeVentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reporte r = new Reporte();
            //r.DsReporte = dsMain;
            r.MdiParent = this;
            r.Show();
        }
    }
}
