﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IConvoProgII
{
    [Serializable]
    class CelularEntitie
    {
        public CelularEntitie() { }

        public CelularEntitie(int tid, string tmarca, string tmodelo, string tred, string tfechaDeLanzamiento, double tmemoria, double tRAM, string tSO, string tversionSO, double tprecio)
        {
            id = tid;
            marca = tmarca;
            modelo = tmodelo;
            red = tred;
            fechaDeLanzamiento = tfechaDeLanzamiento;
            memoria = tmemoria;
            RAM = tRAM;
            SO = tSO;
            versionSO = tversionSO;
            precio = tprecio;
        }

        public int id { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string red { get; set; }
        public string fechaDeLanzamiento { get; set; }
        public double memoria { get; set; }
        public double RAM { get; set; }
        public string SO { get; set; }
        public string versionSO { get; set; }
        public double precio { get; set; }

    }
}
