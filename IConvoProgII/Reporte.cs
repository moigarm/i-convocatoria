﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IConvoProgII
{
    public partial class Reporte : Form
    {
        private DataSet dsReporte;
        private BindingSource bsReporte;

        public DataSet DsReporte
        {
            get
            {
                return dsReporte;
            }

            set
            {
                dsReporte = value;
            }
        }

        public Reporte()
        {
            InitializeComponent();
            bsReporte = new BindingSource();
        }

        private void Reporte_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
