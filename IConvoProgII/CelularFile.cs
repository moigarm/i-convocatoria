﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace IConvoProgII
{
    [Serializable]
    class CelularFile
    {
        CelularEntitie[] celulares;

        public void Serializar(CelularEntitie c, int size)
        {
            FileStream fs = new FileStream("Celulares.dat", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            CelularFile cf = new CelularFile();
            if (fs.Length != 0)
            {
                cf = (CelularFile)bf.Deserialize(fs);
                CelularEntitie[] temp = new CelularEntitie[size + 1];
                cf.celulares.CopyTo(temp, 0);

                temp[size] = c;
                fs.Position = 0;
                cf.celulares = temp;
                bf.Serialize(fs, cf);
            }
            else
            {
                cf.celulares = new CelularEntitie[1];
                cf.celulares[0] = c;
                bf.Serialize(fs, cf);
            }
            fs.Close();
        }

        public void SerializarDelete(CelularEntitie[] c, int size)
        {
            int index = 0;
            do
            {
                if (size == 0)
                {

                }
                else
                {
                    FileStream fs = new FileStream("Celulares.dat", FileMode.OpenOrCreate);
                    BinaryFormatter bf = new BinaryFormatter();
                    CelularFile cf = new CelularFile();
                    if (fs.Length != 0)
                    {
                        cf = (CelularFile)bf.Deserialize(fs);
                        CelularEntitie[] temp = new CelularEntitie[size];
                        cf.celulares.CopyTo(temp, 0);

                        temp[index] = c[index];

                        fs.Position = 0;
                        cf.celulares = temp;
                        bf.Serialize(fs, cf);

                    }
                    else
                    {
                        cf.celulares = new CelularEntitie[1];
                        cf.celulares[index] = c[index];
                        bf.Serialize(fs, cf);
                    }
                    fs.Close();
                }
                index++;
            } while (index != size);
        }

        public CelularEntitie[] findAll()
        {
            try
            {
                FileStream fs = new FileStream("Celulares.dat", FileMode.OpenOrCreate);
                BinaryFormatter bf = new BinaryFormatter();
                CelularEntitie[] cels = ((CelularFile)bf.Deserialize(fs)).celulares;
                fs.Close();
                return cels;
            }
            catch (SerializationException ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public void update(CelularEntitie c, int index, int size)
        {
            FileStream fs = new FileStream("Celulares.dat", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            CelularFile cf = new CelularFile();

            if (fs.Length != 0)
            {
                cf = (CelularFile)bf.Deserialize(fs);
                CelularEntitie[] temp = new CelularEntitie[size];

                cf.celulares.CopyTo(temp, 0);
                temp[index] = c;
                fs.Position = 0;
                cf.celulares = temp;
                bf.Serialize(fs, cf);
                fs.Close();
            }
        }

    }
}
