﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IConvoProgII
{
    public partial class Form1 : Form
    {
        CelularFile cf = new CelularFile();
        List<CelularEntitie> cels = new List<CelularEntitie>();

        private int counts;
        public Form1()
        {
            InitializeComponent();
            
        }

        public void inicio()
        {
            dgvCel.Rows.Clear();
            if (File.Exists("Celulares.dat"))
            {
                CelularEntitie[] phones = cf.findAll();
                if (phones == null)
                {
                    return;
                }
                foreach (CelularEntitie c in phones)
                {
                    cels.Add(c);
                }
                if (cels.Count > 0)
                {
                    foreach (CelularEntitie c in phones)
                    {
                        dgvCel.Rows.Add(c.id, c.marca, c.modelo,c.red,c.fechaDeLanzamiento,c.memoria,c.RAM,c.SO,c.versionSO,c.precio);
                    }
                }

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            inicio();
        }

        void FrmCelular_FormClosed(object sender, FormClosedEventArgs e)
        {
            inicio();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            int idUL = 0;

            counts = dgvCel.Rows.Count;

            if (counts != 0)
            {
                string columna1 = dgvCel.Rows[counts - 1].Cells[0].Value.ToString();
                idUL = Convert.ToInt32(columna1);
            }

            FrmCelular f = new FrmCelular();
            f.IdUl = idUL;
            f.Edit = false;
            f.SizeArray = counts;

            f.FormClosed += new FormClosedEventHandler(FrmCelular_FormClosed);

            f.ShowDialog();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            counts = dgvCel.Rows.Count;

            if (counts == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string idcell = dgvCel.CurrentRow.Cells[0].Value.ToString();
            int id = Convert.ToInt32(idcell);

            int n = 0;
            int currentIndex = 0;
            string marca = "", modelo = "", red = "", fecha = "";
            double memoria = 0, ram = 0, precio = 0;
            string so = "", version = "";
            foreach (DataGridViewRow i in dgvCel.Rows)
            {
                if (Convert.ToInt32(i.Cells[0].Value) == id)
                {
                    currentIndex = n;
                    marca = i.Cells[1].Value.ToString();
                    modelo = i.Cells[2].Value.ToString();
                    red = i.Cells[3].Value.ToString();
                    fecha = i.Cells[4].Value.ToString();
                    memoria = Convert.ToDouble(i.Cells[5].Value);
                    ram = Convert.ToDouble(i.Cells[6].Value);
                    so = i.Cells[7].Value.ToString();
                    version = i.Cells[8].Value.ToString();
                    precio = Convert.ToDouble(i.Cells[9].Value);
                    
                }
                ++n;

            }

            FrmCelular f = new FrmCelular();
            f.Id = id;
            f.Marca = marca;
            f.Modelo = modelo;
            f.Red = red;
            f.Fecha = fecha;
            f.Memoria = memoria;
            f.Ram = ram;
            f.So = so;
            f.Version = version;
            f.Precio = precio;

            f.Pos = currentIndex;
            f.Edit = true;
            f.SizeArray = counts;

            f.FormClosed += new FormClosedEventHandler(FrmCelular_FormClosed);

            f.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            counts = dgvCel.Rows.Count;
            if (counts == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder eliminar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string idcell = dgvCel.CurrentRow.Cells[0].Value.ToString();
            int id = Convert.ToInt32(idcell);

            CelularEntitie[] ce = cf.findAll();

            File.Delete("Celulares.dat");

            if (ce.Length != 1)
            {

                int currentIndex = 0;
                int n = 0;
                foreach (DataGridViewRow i in dgvCel.Rows)
                {
                    if (Convert.ToInt32(i.Cells[0].Value) == id)
                    {
                        currentIndex = n;

                    }
                    ++n;

                }
                List<CelularEntitie> phoneTemp = ce.ToList<CelularEntitie>();

                phoneTemp.RemoveAt(currentIndex);

                CelularEntitie[] arrayTemp = phoneTemp.ToArray();

                cf.SerializarDelete(arrayTemp, counts - 1);
            }

            inicio();
        }
    }
}
