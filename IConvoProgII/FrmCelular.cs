﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace IConvoProgII
{
    public partial class FrmCelular : Form
    {

        CelularFile cf = new CelularFile();

        private int idUl;
        private bool edit;
        private int id;
        private string marca;
        private string modelo;
        private string red;
        private string fecha;
        private double memoria;
        private double ram;
        private string so;
        private string version;
        private double precio;
        
        public FrmCelular()
        {
            InitializeComponent();
            
        }

        public int IdUl { get => idUl; set => idUl = value; }
        public bool Edit { get => edit; set => edit = value; }
        public int Id { get => id; set => id = value; }
        public string Marca { get => marca; set => marca = value; }
        public string Modelo { get => modelo; set => modelo = value; }
        public string Red { get => red; set => red = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public double Memoria { get => memoria; set => memoria = value; }
        public double Ram { get => ram; set => ram = value; }
        public string So { get => so; set => so = value; }
        public string Version { get => version; set => version = value; }
        public double Precio { get => precio; set => precio = value; }

        private int pos;
        public int Pos { get => pos; set => pos = value; }

        
        private int sizeArray;

        public int SizeArray { get => sizeArray; set => sizeArray = value; }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string marca, modelo, sRed = "";
            double mI = 0, RAM = 0,precio = 0;
            string so = "", version = "";


            DateTime fecha = dtpickerFecha.Value;
            if (cmbRed.SelectedIndex == 0)
            {
                sRed = "3G";
            }
            else if (cmbRed.SelectedIndex == 1)
            {
                sRed = "4G";
            }
            else if (cmbRed.SelectedIndex == 2)
            {
                sRed = "5G";
            }

            if (cmbSO.SelectedIndex == 0)
            {
                so = "Android";
            }
            else if(cmbSO.SelectedIndex == 1)
            {
                so = "Ios";
            }

            try
            {
                marca = txtMarca.Text;
                modelo = txtModelo.Text;


                mI = Convert.ToDouble(txtMemoriaInterna.Text);
                RAM = Convert.ToDouble(txtRAM.Text);

                version = txtVersionSO.Text;
                precio = Convert.ToDouble(txtPrecio.Text);

            }
            catch (FormatException)
            {
                MessageBox.Show(this, "ERROR, debe llenar los datos que se le pide para poder agregar el celular", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cmbRed.SelectedIndex == -1)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar red para poder agregar el celular", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cmbSO.SelectedIndex == -1)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar sistema operativo para poder agregar el celular", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Edit == true)
            {
                CelularEntitie c = new CelularEntitie();
                c.id = Id;
                c.marca = marca;
                c.modelo = modelo;
                c.red = sRed;
                c.fechaDeLanzamiento = fecha.ToShortDateString();
                c.memoria = mI;
                c.RAM = RAM;
                c.SO = so;
                c.versionSO = version;
                c.precio = precio;
                cf.update(c, Pos, SizeArray);

            }
            else
            {
                CelularEntitie c = new CelularEntitie();
                c.id = IdUl + 1;
                c.marca = marca;
                c.modelo = modelo;
                c.red = sRed;
                c.fechaDeLanzamiento = fecha.ToShortDateString();
                c.memoria = mI;
                c.RAM = RAM;
                c.SO = so;
                c.versionSO = version;
                c.precio = precio;
                cf.Serializar(c, SizeArray);

            }

            Dispose();
        }

        private void FrmCelular_Load(object sender, EventArgs e)
        {
            if (Edit == true)
            {
                txtMarca.Text = Marca;
                txtModelo.Text = Modelo;
                if (Red == "3G")
                {
                    cmbRed.SelectedIndex = 0;
                }
                else if (Red == "4G")
                {
                    cmbRed.SelectedIndex = 1;
                }
                else if (Red == "5G")
                {
                    cmbRed.SelectedIndex = 2;
                }
                dtpickerFecha.Value = Convert.ToDateTime(Fecha);
                txtMemoriaInterna.Text = Memoria.ToString();
                txtRAM.Text = Ram.ToString();
                if (So == "Android")
                {
                    cmbSO.SelectedIndex = 0;
                }
                else if (So == "Ios")
                {
                    cmbSO.SelectedIndex = 1;
                }
                txtVersionSO.Text = Version;
                txtPrecio.Text = Precio.ToString();
            }

        }
    }
}

